import React, { Component } from 'react';
// import { Container, Header, Content, Footer, Text } from 'native-base';

import { createStackNavigator, createAppContainer } from "react-navigation";
import HomeScreen from './components/HomeScreen';
import ResultScreen from './components/ResultScreen';

export default class App extends Component {
  render() {
    return <AppNavigator />;
  }
}
const AppNavigator = createAppContainer(createStackNavigator({
    Home: {screen: HomeScreen, 
      navigationOptions: {
        header: null
      }
      },
    Result: {screen: ResultScreen ,
        navigationOptions:{
          header : null
        }
     }
  }));