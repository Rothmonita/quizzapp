import React, { Component } from 'react';
  import { 
    Container, 
    Header,
    ListItem, 
    Title, 
    Content, 
    Footer,
    Card, 
    CardItem, 
    Radio, 
    Text, 
    Left,
    FooterTab, 
    Button, 
    Right, 
    Body,
    View } from 'native-base';
  //import { ScrollView } from 'react-native-gesture-handler';


const questions=[
          {
              text: "What is the name of the US president?", 
              correct: 1, 
              answers: [
                  "Obama", 
                  "Trump", 
                  "Roosvelt", 
                  "Putin"
              ]
          }, 
          {
              text: "What is the square root of 100?", 
              correct: 3, 
              answers: [
                  "sin(10)", 
                  "1", 
                  "100", 
                  "10"
              ]
          }
      ]
  
export default class HomeScreen extends Component {
    constructor(){
      super()
      this.state={
        selectedAnswer:[],
        score :0
      }

    }
    

    selectQuestion(q,a){
      let answer = this.state.selectedAnswer;
      answer[q]=a;
      this.setState({
        selectQuestion:answer
      })
     }
    
     checkQuestion(){
      let allResult = this.state.answer
      var totalScore = 0;
      for(var i=0;i<questions.length;i++){
        if(questions[i].correct === allResult[i]) totalScore += 1;
      }

      this.props.navigation.navigate('ResultsScreen',{
        TotalScores: totalScore,
        TotalQuestions: questions.length
      });
       
     }


 render() {
      return (
        <Container>
          <Header>
            <Left>
              <Button transparent>
                {/* <Icon name='menu' /> */}
              </Button>
            </Left>
            <Body>
              <Title>My Quiz</Title>
            </Body>
            <Right />
          </Header>
          <Content>
            {
                questions.map((all, qustionIndex = i ) =>{
                     return(
                       <Card key={qustionIndex}>
                          <CardItem>
                              <Body>
                                 <View>
                                     <Text> {all.text}  </Text>
                                     {
                                       all.answers.map((answer, answerIndex=i)=>{
                                         return(
                                           <View key={answerIndex}>
                                           <ListItem selected={this.state.radioSelected} onPress={()=>this.selectQuestion(qustionIndex,answerIndex)}>
                                           <Left><Text style={{paddingLeft:15}}>{answer}</Text></Left>
                                              <Right>
                                                  <Radio selected={this.state.selected}
                                                          onPress={()=>this.selectQuestion(qustionIndex,answerIndex)}
                                                   />
                                              </Right>
                                             
                                           </ListItem>
                                           </View>
                                         )
                                       })
                                     }
                                 </View>
                              </Body>
                          </CardItem>
                       </Card>
                     )
                }
              )

            }
            <Card padding>
              
                <Button full onPress={()=>this.props.navigation.navigate('Result')}>
                  <Text>Submit All Answer</Text>
                </Button>
            </Card>
          </Content>

          
        </Container>
      );
   }
}