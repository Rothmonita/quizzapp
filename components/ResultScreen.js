import React, { Component } from 'react';
import { View, Text } from 'react-native';

class ResultScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const totalQuestions = this.props.navigation.state.params.TotalQuestions;
    const score =this.props.navigation.state.params.TotalScores;
    let AllScore=(score*totalQuestions)/100;
    return (
      <View style={{flex:1,justifyContent:"center",alignItems:'center'}}>
        <Text> Your score is: {score}/{TotalQuestions} and equal to {AllScore}%</Text>
      </View>
    );
  }
}

export default ResultScreen;
